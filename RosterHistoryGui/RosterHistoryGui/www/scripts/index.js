﻿let league = document.getElementById("league");
let teams = document.getElementById("team");
let start_year = document.getElementById("start-year")
let end_year = document.getElementById("end-year")
let display_rosters = document.getElementById("rosters")
let get_rosters = document.getElementById("get-ros-his")
let nba_teams = ["Atlanta Hawks", "Boston Celtics", "Brooklyn Nets", "Charlotte Hornets", "Chicago Bulls", "Cleveland Cavaliers",
    "Dallas Mavericks", "Denver Nuggets", "Detroit Pistons", "Golden State Warriors", "Houston Rockets", "Indiana Pacers",
    "Los Angeles Clippers", "Los Angeles Lakers", "Memphis Grizzlies", "Miami Heat", "Milwaukee Bucks", "Minnesota Timberwolves",
    "New Orleans Pelicans", "New York Knicks", "Oklahoma City Thunder", "Orlando Magic", "Philadelphia 76ers", "Phoenix Suns",
    "Portland Trail Blazers", "Sacramento Kings", "San Antonio Spurs", "Toronto Raptors", "Utah Jazz", "Washington Wizards"];
let wnba_teams = ["Atlanta Dream", "Chicago Sky", "Connecticut Sun", "Dallas Wings", "Indiana Fever", "Las Vegas Aces",
    "Los Angeles Sparks", "Minnesota Lynx", "New York Liberty", "Phoenix Mercury", "Seattle Storm", "Washington Mystics"];
let stat_names = ["GP", "GS", "MPG", "FG%", "3P%", "FT%", "RPG", "APG", "SPG", "BPG", "PPG"];



function NbaStartYear() {
    // Sets the start year options from 1946 - current year if NBA is selected
    start_year.innerHTML = "";
    for (i = 2021; i >= 1946; i--) {
        var opt = document.createElement("option");
        opt.value = i;
        opt.innerText = i;
        start_year.appendChild(opt);
    }
};

function WnbaStartYear() {
    // Sets the start year options from 1997 - current year if WNBA is selected
    start_year.innerHTML = "";
    for (i = 2021; i >= 1997; i--) {
        var opt = document.createElement("option");
        opt.value = i;
        opt.innerText = i;
        start_year.appendChild(opt);
    }
};

function EndYear() {
    // Sets the end year options from start year + 1 to current year + 1
    end_year.innerHTML = "";
    for (i = 2022; i > start_year.value; i--) {
        var opt = document.createElement("option");
        opt.value = i;
        opt.innerText = i;
        end_year.appendChild(opt);
    }
};

start_year.addEventListener("change", EndYear, false);

league.addEventListener("change", function () {
    // Display correct teams for league selected
    teams.innerHTML = "";
    if (league.value == "NBA") {
        for (var i = 0; i < nba_teams.length; i++) {
            var opt = document.createElement("option");
            opt.value = nba_teams[i];
            opt.innerText = nba_teams[i];
            teams.appendChild(opt);
        }
        NbaStartYear();
    } else {
        for (var i = 0; i < wnba_teams.length; i++) {
            var opt = document.createElement("option");
            opt.value = wnba_teams[i];
            opt.innerText = wnba_teams[i];
            teams.appendChild(opt);
        }
        WnbaStartYear();
    }
    EndYear();
}, false);

document.addEventListener("DOMContentLoaded", function () {
    // Initialize dropdowns with NBA information
    teams.innerHTML = "";
    for (var i = 0; i < nba_teams.length; i++) {
        var opt = document.createElement("option");
        opt.value = nba_teams[i];
        opt.innerText = nba_teams[i];
        teams.appendChild(opt);
    }
    NbaStartYear();
    EndYear();
});

function DisplayRosters(rost_data) {
    display_rosters.innerHTML = ""
    var results_header = document.createElement("h3");
    results_header.id = "results-header";

    if (rost_data.code != 200) {
        results_header.innerText = `${rost_data.message}`;
        display_rosters.appendChild(results_header);
        return
    }

    var start = rost_data.start;
    start += 1;
    var end = rost_data.end;

    results_header.innerText = `${rost_data.city} ${rost_data.team} rosters from ${start} to ${end}`;
    display_rosters.appendChild(results_header);

    for (i = start; i < end; i++) {
        var prev = []
        var cur = []
        var next = []
        if (rost_data.hasOwnProperty(String(+i - 1))) {
            prev = rost_data[String(+i - 1)];
        }; 
        if (rost_data.hasOwnProperty(String(+i))) {
            cur = rost_data[String(+i)];
        }; 
        if (rost_data.hasOwnProperty(String(+i + 1))) {
            next = rost_data[String(+i + 1)];
        }; 

        // Create div for roster year
        var div_year = document.createElement("div");
        div_year.classList.add("ros-yr");
        div_year.id = `${+i}`

        // Add year range of season 
        var p_year = document.createElement("p");
        p_year.classList.add("year");
        p_year.innerText = `${+i} - ${+i + 1}`;
        div_year.appendChild(p_year);

        // Create list of players on the team that season
        var ul = document.createElement("ul");
        ul.id = `${rost_data.city} ${rost_data.team}`;
        for (var j = 0; j < cur.length; j++) {
            var player = cur[j];
            var li = document.createElement("li");

            // Check if new player
            var div_join = document.createElement("div");
            div_join.classList.add("join-div");
            if (!prev.includes(player)) {
                var new_arrow = document.createElement("i");
                new_arrow.classList.add("new");
                div_join.appendChild(new_arrow);
            };
            li.appendChild(div_join);

            // Add player
            var div_player = document.createElement("div");
            div_player.classList.add("player-div");
            div_player.innerText = player;
            //div_player.addEventListener("click", ViewStats(div_player))
            li.appendChild(div_player);

            // Check if player leaves
            var div_leave = document.createElement("div");
            div_leave.classList.add("leave-div");
            if (!next.includes(player)) {
                var leave_arrow = document.createElement("i");
                leave_arrow.classList.add("leaving");
                div_leave.appendChild(leave_arrow);
            };
            li.appendChild(div_leave);

            // Add stats div
            var div_stats = document.createElement("div");
            div_stats.classList.add("stats");
            div_stats.classList.add("hidden");
            p_name = player
            p_name = p_name.replace(/\s/g, '')
            div_stats.id = `${p_name}${+i}`;
            li.appendChild(div_stats);

            ul.appendChild(li);
        }
        div_year.appendChild(ul);
        display_rosters.appendChild(div_year);
    }
};


get_rosters.onclick = ("click", GetRosterHistory)


function GetRosterHistory() {
    display_rosters.innerHTML = ""
    var results_header = document.createElement("h3");
    results_header.id = "results-header";
    results_header.innerText = `Looking up roster information. Please allow 20 - 30 seconds for each season requested.`;
    display_rosters.appendChild(results_header);

    get_rosters.disabled = true;

    var city_team = teams.value;
    city_team = city_team.split(" ");
    let req = new XMLHttpRequest();

    //req.open('GET', `http://rosterhistory.loafing.dev/api/roster-history?league=${league.value}&city=${city_team[0]}&team=${city_team[1]}&start=${start_year.value-1}&end=${end_year.value}`, true);
    req.open('GET', `http://localhost:2143/api/roster-history?league=${league.value}&city=${city_team[0]}&team=${city_team[1]}&start=${start_year.value-1}&end=${end_year.value}`, true);
    req.addEventListener('load', function () {
        let response = JSON.parse(req.responseText);
        
        DisplayRosters(response)
        get_rosters.disabled = false;
    });
    req.send();
};

// Detect if a player's name is clicked to show stats
document.addEventListener('click', function (e) {
    e = e || window.event;
    var element_clicked = e.target || e.srcElement
    var player_name = ""
    var season = ""
    if (element_clicked.classList.contains('player-div')) {
        player_name = element_clicked.textContent || element_clicked.innerText;
        player_name = player_name.split(" ");
        player_data = {};
        player_data.f_name = player_name[0];
        player_data.l_name = player_name[1];
        player_data.team = element_clicked.parentElement.parentElement.id;
        player_data.season = element_clicked.parentElement.parentElement.parentElement.id;
        player_data.stats_id = `${player_data.f_name}${player_data.l_name}${player_data.season}`;
        ViewStats(player_data);
    }
}, false);

// Show or hide stats
function ViewStats(player_data) {
    var stats_div = document.getElementById(player_data.stats_id);
    stats_div.classList.toggle("hidden");
    if (stats_div.innerText == "") {
        GetPlayerStats(player_data, stats_div);
    }
}

// Get stats for a player from the backend
function GetPlayerStats(player_data, stats_div) {
    let req = new XMLHttpRequest();
    //req.open('GET', `http://rosterhistory.loafing.dev/api/player-stats?f_name=${player_data.f_name}&l_name=${player_data.l_name}&year=${player_data.season}`, true);
    req.open('GET', `http://localhost:2143/api/player-stats?f_name=${player_data.f_name}&l_name=${player_data.l_name}&year=${player_data.season}`, true);
    req.addEventListener('load', function () {
        let response = JSON.parse(req.responseText);
        AddStats(player_data.team, response, stats_div);
    });
    req.send();
};

// Add stats to the page to be displayed
function AddStats(player_team, stats, stats_div) {
    if (stats.stats == "Stats not found") {
        stats_div.innerText = "Stats not found";
        return
    };
        var team_found = false;

    // Find stats while with selected team and display them if existing
    for (item in stats.stats) {
        var team_name = stats.stats[item].team;
        team_name = team_name.trim();
        if (team_name == player_team) {
            var counter = 0;
            for (stat_name in stat_names) {
                var stat_span = document.createElement("span");
                stat_span.classList.add("stat")
                stat_span.innerHTML = `<span class="stat-name">${stat_names[stat_name]}:</span> ${stats.stats[item][stat_names[stat_name]]}`
                stats_div.appendChild(stat_span);
                counter++;
                if (counter % 4 == 0) {
                    var br = document.createElement("br");
                    stats_div.appendChild(br);
                }
            }
            team_found = true;
        };

        if (team_found) {
            break;
        }
    };

    if (!team_found) {
        stats_div.innerText = "Stats not found";
    }
}

document.getElementById("help-btn").addEventListener("click", function () {
    window.location = "./help.html";
});